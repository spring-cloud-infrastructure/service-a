package com.spring.cloud.messaging.sender

import com.spring.cloud.config.messaging.MessagingProperties
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage
import mu.KotlinLogging.logger
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.stereotype.Service

@Service
internal class MessagingSenderService(
    private val messagingProperties: MessagingProperties,
    private val amqpTemplate: AmqpTemplate,
) {

    private val logger = logger { }

    fun enqueueToServiceExchange(message: HelloServiceBMessage) =
        enqueueToServiceExchange(messagingProperties.binding.helloServiceB.routingKey, message)

    private fun enqueueToServiceExchange(routingKey: String, message: Any) {
        logger.debug { "Enqueuing message: [${message.javaClass.simpleName}] to service exchange: [${messagingProperties.serviceExchange}] with routing key: [$routingKey]" }
        amqpTemplate.convertAndSend(messagingProperties.serviceExchange, routingKey, message)
    }
}
