package com.spring.cloud.messaging.sender.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank

data class HelloServiceBMessage(
    @JsonProperty("message") @NotBlank val message: String,
)
