package com.spring.cloud.config.messaging

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import javax.validation.constraints.NotBlank

@ConstructorBinding
@ConfigurationProperties("services.service-a.messaging")
data class MessagingProperties(
    @NotBlank val serviceExchange: String,
    val binding: BindingProperties,
) {

    data class BindingProperties(
        val helloServiceB: BindingProperty,
    )

    data class BindingProperty(
        @NotBlank val queue: String,
        @NotBlank val routingKey: String,
    )
}
