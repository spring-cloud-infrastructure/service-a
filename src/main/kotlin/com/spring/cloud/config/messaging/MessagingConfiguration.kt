package com.spring.cloud.config.messaging

import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder.bind
import org.springframework.amqp.core.Exchange
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableRabbit
internal class MessagingConfiguration {

    // TODO: Use queue schema registry if possible
    // https://github.com/spring-cloud/spring-cloud-schema-registry
    // https://github.com/spring-cloud/spring-cloud-stream-samples
    // https://github.com/EugeneMsv/amqp-rabbit-spring-boot-autoconfigure
    @Bean("helloServiceBQueue")
    fun helloServiceBQueue(messagingProperties: MessagingProperties): Queue =
        Queue(messagingProperties.binding.helloServiceB.queue)

    @Bean("serviceExchange")
    fun serviceExchange(messagingProperties: MessagingProperties): Exchange =
        TopicExchange(messagingProperties.serviceExchange)

    @Bean("serviceExchangeToHelloServiceBQueueBinding")
    fun serviceExchangeToHelloServiceBQueueBinding(
        messagingProperties: MessagingProperties,
        @Qualifier("helloServiceBQueue") helloServiceBQueue: Queue,
        @Qualifier("serviceExchange") serviceExchange: Exchange,
    ): Binding = bind(helloServiceBQueue)
        .to(serviceExchange)
        .with(messagingProperties.binding.helloServiceB.routingKey)
        .noargs()

    @Bean
    fun messagingJsonConverter(): MessageConverter = Jackson2JsonMessageConverter()
}
