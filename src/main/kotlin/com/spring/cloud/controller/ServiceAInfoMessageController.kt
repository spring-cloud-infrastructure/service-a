package com.spring.cloud.controller

import com.spring.cloud.messaging.sender.MessagingSenderService
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage
import com.spring.cloud.resourceserver.client.authority.service.a.HasReadInfoMessageAAuthority
import mu.KotlinLogging.logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RefreshScope
internal class ServiceAInfoMessageController(
    @Value("\${services.service-a.controller.ServiceAInfoMessageController.info-message}") private val infoMessage: String,
    private val messagingSenderService: MessagingSenderService,
) {

    private val logger = logger { }

    @GetMapping("info-message")
    @HasReadInfoMessageAAuthority
    fun getInfoMessage(authentication: Authentication): ResponseEntity<String> {
        logger.info { "User: [${authentication.name}] with granted authorities: ${authentication.authorities} is accessing info message" }
        messagingSenderService.enqueueToServiceExchange(HelloServiceBMessage(infoMessage))
        return ok(infoMessage)
    }
}
