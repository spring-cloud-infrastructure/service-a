package com.spring.cloud.controller

import com.spring.cloud.messaging.sender.MessagingSenderService
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage
import com.spring.cloud.resourceserver.client.authority.service.a.HasScheduledJobAuthority
import mu.KotlinLogging.logger
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("schedulers")
internal class SchedulerController(private val messagingSenderService: MessagingSenderService) {

    private val logger = logger { }

    @PostMapping("hello-world")
    @HasScheduledJobAuthority
    fun helloWorld(authentication: Authentication): ResponseEntity<String> {
        logger.info { "User: [${authentication.name}] with granted authorities: ${authentication.authorities} is accessing scheduled job" }
        val message = "Hi buddy!"
        messagingSenderService.enqueueToServiceExchange(HelloServiceBMessage(message))
        return ok(message)
    }
}
