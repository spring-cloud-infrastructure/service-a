package com.spring.cloud.messaging.sender

import com.spring.cloud.config.messaging.MessagingProperties
import com.spring.cloud.config.messaging.MessagingProperties.BindingProperties
import com.spring.cloud.config.messaging.MessagingProperties.BindingProperty
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.amqp.core.AmqpTemplate

internal class MessagingSenderServiceTest {

    private val messagingProperties = mockk<MessagingProperties>()
    private val amqpTemplate = mockk<AmqpTemplate>()
    private val bindings = mockk<BindingProperties>()
    private val helloServiceBBindings = mockk<BindingProperty>()

    private val underTest = MessagingSenderService(messagingProperties, amqpTemplate)

    @Test
    fun `given hello service b message when message is send to queue then message is enqueued to service exchange under hello service b routing key`() {
        val helloServiceBMessage = HelloServiceBMessage(message = "test message 123")
        val routingKey = "routing-key"
        val serviceExchange = "service-exchange"

        every { messagingProperties.binding } returns bindings
        every { bindings.helloServiceB } returns helloServiceBBindings
        every { helloServiceBBindings.routingKey } returns routingKey
        every { messagingProperties.serviceExchange } returns serviceExchange
        justRun { amqpTemplate.convertAndSend(serviceExchange, routingKey, helloServiceBMessage) }

        underTest.enqueueToServiceExchange(helloServiceBMessage)

        verify {
            messagingProperties.serviceExchange
            messagingProperties.binding
            bindings.helloServiceB
            helloServiceBBindings.routingKey
            amqpTemplate.convertAndSend(serviceExchange, routingKey, helloServiceBMessage)
        }
    }
}
