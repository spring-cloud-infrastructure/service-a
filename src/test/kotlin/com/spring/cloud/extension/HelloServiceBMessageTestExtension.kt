package com.spring.cloud.extension

import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.assertions.prop
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage

fun Assert<HelloServiceBMessage>.hasMessageEqualTo(message: String): Assert<HelloServiceBMessage> {
    prop("message") { it.message }.isEqualTo(message)
    return this
}
