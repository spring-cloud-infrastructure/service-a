package com.spring.cloud.controller

import assertk.assertThat
import com.ninjasquad.springmockk.MockkBean
import com.spring.cloud.config.messaging.MessagingProperties
import com.spring.cloud.extension.hasMessageEqualTo
import com.spring.cloud.messaging.sender.MessagingSenderService
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage
import io.mockk.justRun
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post

@WebMvcTest(SchedulerController::class)
@Import(MessagingSenderService::class)
@EnableConfigurationProperties(MessagingProperties::class)
internal class SchedulerControllerIntegrationTest(
    @Autowired private val messagingProperties: MessagingProperties,
    @Autowired private val mockMvc: MockMvc,
) {

    companion object {
        private const val SCHEDULED_JOB_AUTHORITY = "SCHEDULED_JOB"
        private const val HELLO_WORLD_URL = "/schedulers/hello-world"
        private const val HELLO_SERVICE_B_MESSAGE = "Hi buddy!"
    }

    @MockkBean
    private lateinit var amqpTemplate: AmqpTemplate

    @Test
    fun `given user is not authorized when hello world endpoint is called then 403 is returned`() {
        mockMvc.post(HELLO_WORLD_URL) { }
            .andDo { print() }
            .andExpect { status { isForbidden() } }
    }

    @Test
    fun `given user is authorized but has no SCHEDULED_JOB authority when hello world endpoint is called then 403 is returned`() {
        val authorization = jwt()

        mockMvc.post(HELLO_WORLD_URL) { with(authorization) }
            .andDo { print() }
            .andExpect { status { isForbidden() } }
    }

    @Test
    fun `given user is authorized and has SCHEDULED_JOB authority when hello world endpoint is called then 200 and message are returned`() {
        justRun {
            amqpTemplate.convertAndSend(
                messagingProperties.serviceExchange,
                messagingProperties.binding.helloServiceB.routingKey,
                any<HelloServiceBMessage>()
            )
        }

        val authorization = jwt()
            .authorities(SimpleGrantedAuthority(SCHEDULED_JOB_AUTHORITY))

        mockMvc.post(HELLO_WORLD_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { string(HELLO_SERVICE_B_MESSAGE) }
            }
    }

    @Test
    fun `given user is authorized and has SCHEDULED_JOB authority when hello world endpoint is called then hello service B message is enqueued into service exchange`() {
        justRun {
            amqpTemplate.convertAndSend(
                messagingProperties.serviceExchange,
                messagingProperties.binding.helloServiceB.routingKey,
                any<HelloServiceBMessage>()
            )
        }

        val authorization = jwt()
            .authorities(SimpleGrantedAuthority(SCHEDULED_JOB_AUTHORITY))

        mockMvc.post(HELLO_WORLD_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { string(HELLO_SERVICE_B_MESSAGE) }
            }

        verify {
            amqpTemplate.convertAndSend(
                messagingProperties.serviceExchange,
                messagingProperties.binding.helloServiceB.routingKey,
                withArg<HelloServiceBMessage> {
                    assertThat(it).hasMessageEqualTo(HELLO_SERVICE_B_MESSAGE)
                }
            )
        }
    }
}
