package com.spring.cloud.controller

import assertk.assertThat
import com.ninjasquad.springmockk.MockkBean
import com.spring.cloud.config.messaging.MessagingProperties
import com.spring.cloud.extension.hasMessageEqualTo
import com.spring.cloud.messaging.sender.MessagingSenderService
import com.spring.cloud.messaging.sender.model.HelloServiceBMessage
import io.mockk.justRun
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.cloud.autoconfigure.RefreshAutoConfiguration
import org.springframework.context.annotation.Import
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@WebMvcTest(ServiceAInfoMessageController::class)
@ImportAutoConfiguration(RefreshAutoConfiguration::class)
@EnableConfigurationProperties(MessagingProperties::class)
@Import(MessagingSenderService::class)
internal class ServiceAInfoMessageControllerIntegrationTest(
    @Value("\${services.service-a.controller.ServiceAInfoMessageController.info-message}") private val infoMessage: String,
    @Autowired private val messagingProperties: MessagingProperties,
    @Autowired private val mockMvc: MockMvc,
) {

    companion object {
        private const val READ_INFO_MESSAGE_A_AUTHORITY = "READ_INFO_MESSAGE_A"
        private const val INFO_MESSAGE_URL = "/info-message"
    }

    @MockkBean
    private lateinit var amqpTemplate: AmqpTemplate

    @Test
    fun `given user is not authorized when info message endpoint is called then 401 is returned`() {
        mockMvc.get(INFO_MESSAGE_URL) { }
            .andDo { print() }
            .andExpect { status { isUnauthorized() } }
    }

    @Test
    fun `given user is authorized but has no READ_INFO_MESSAGE_A authority when info message endpoint is called then 403 is returned`() {
        val authorization = jwt()

        mockMvc.get(INFO_MESSAGE_URL) { with(authorization) }
            .andDo { print() }
            .andExpect { status { isForbidden() } }
    }

    @Test
    fun `given user is authorized and has READ_INFO_MESSAGE_A authority when info message endpoint is called then 200 and info message are returned`() {
        justRun {
            amqpTemplate.convertAndSend(
                messagingProperties.serviceExchange,
                messagingProperties.binding.helloServiceB.routingKey,
                any<HelloServiceBMessage>()
            )
        }

        val authorization = jwt().authorities(SimpleGrantedAuthority(READ_INFO_MESSAGE_A_AUTHORITY))

        mockMvc.get(INFO_MESSAGE_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { string(infoMessage) }
            }
    }

    @Test
    fun `given user is authorized and has READ_INFO_MESSAGE_A authority when info message endpoint is called then hello service B message is enqueued into service exchange`() {
        justRun {
            amqpTemplate.convertAndSend(
                messagingProperties.serviceExchange,
                messagingProperties.binding.helloServiceB.routingKey,
                any<HelloServiceBMessage>()
            )
        }

        val authorization = jwt().authorities(SimpleGrantedAuthority(READ_INFO_MESSAGE_A_AUTHORITY))

        mockMvc.get(INFO_MESSAGE_URL) { with(authorization) }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content { string(infoMessage) }
            }

        verify {
            amqpTemplate.convertAndSend(
                messagingProperties.serviceExchange,
                messagingProperties.binding.helloServiceB.routingKey,
                withArg<HelloServiceBMessage> {
                    assertThat(it).hasMessageEqualTo(infoMessage)
                }
            )
        }
    }
}
