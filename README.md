# Service A

Dummy business service.

## 1. Features

- Scheduled task [ServiceAScheduledJob](./src/main/java/com/spring/cloud/scheduled/ServiceAScheduledJob.java) which runs
  every 10 seconds.

## 2. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

## 3. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
